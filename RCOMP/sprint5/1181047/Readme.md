# REPORT RCOMP #

# Protect Communication Between SCM And Machines #

### FUNCIONAMENTO DA COMUNICAÇÃO ###

* Com o aumento da utilização da Internet para fins comerciais, tornou-se imprescindível a criação de meios que possibilitassem a comunicação através da rede de forma segura. Utilizando o SSL (Secure Socket Layer), podemos garantir uma autenticação sólida entre duas aplicações de rede, neste caso, entre o simulador de máquinas e o SCM. Este permite que o cliente/servidor possam trocar informações de forma totalmente segura, protegendo a integridade e a veracidade do conteúdo que circula na Internet. Tal segurança só é possível através da autenticação das partes envolvidas na troca de informações.

* Ambas as aplicações necessitam de ter um certificado de chave pública. Não é relevante se a aplicação é cliente ou servidor, no entanto é necessário ter presente que por omissão o servidor não exige um certificado de chave pública ao cliente, logo para haver autenticação mútua é necessário que a aplicação servidora solicite o certificado do cliente.
* Geramos os certificados, através do make_certs para que seja feita uma comunicação SSL entre as aplicações:

    * Em C/OpenSSL: SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER|SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);
    * Em Java: setNeedClientAuth(true);

* Cada aplicação tem de ter um certificado de chave pública (e a correspondente chave privada);
    * Para gerir chaves e certificados de chave pública da aplicação C/OpenSSL deve ser usado o comando openssl
        * openssl req -config ssl-tmp.cnf -new -x509 -days 3650 -nodes -sha256 -out ${name}.pem -keyout ${name}.key
    * Para gerir chaves e certificados de chave pública da aplicação Java deve ser usado o comando keytool
        * keytool -genkey -v -alias ${name} -keyalg RSA -keysize 2048 -validity 365 -keystore ${name}.jks -storepass ${STOREPASS}

* Estes certificados são auto assinados e a informação documental neles constante não é relevante porque vão ser manualmente fornecidos às aplicações, o importante é mesmo a chave pública. Depois disto serão gerados os ficheiros     .pem, .key e .jks

* Na aplicação C/OpenSSL, a chave privada e o certificado da aplicação são definidos através das funções:
    * SSL_CTX_use_certificate_file(…, “client1.pem” , …)
    * SSL_CTX_use_PrivateKey_file(…, “client1.key”, … )

* Para que as aplicações se aceitem mutuamente como autênticas é necessário que:
    * No SCM, os certificados de confiança incluam o certificado de chave pública do simulador da máquina
    * No simulador da máquina, os certificados de confiança incluam o certificado de chave pública do SCM

* O certificado de chave pública do simulador da máquina está na key store B.jks, mas pode ser exportado em formato PEM através do comando:
    * keytool -exportcert -alias ${name} -keystore ${name}.jks -storepass ${STOREPASS} -rfc -file ${name}.pem
* O ficheiro .pem tem de ser fornecido ao SCM, sendo assim o único certificado aceite pelo SCM:
    * SSL_CTX_load_verify_locations(ctx, ”server_J.pem”, NULL).

### PLANO DE TESTES FUNCIONAIS ###

#### CASO #1 : Mandar Mensagens Hello Com Sucesso ####

1. Gerar os certificados através da execução do comando ./make_certs no gitbash.
2. Executar o servidor do Sistema Central de Comunicação com as máquinas.
3. Executar o simulador da máquina (por exemplo, se a linha de produção escolhida for a *LP_002*, executar a máquina 10).
4. O simulador da máquina envia um *hello request* ao servidor.
5. O servidor recebe o pedido e retorna um *ACK*, visto que a máquina pertence à linha de produção identificada no servidor e tem um certificado conhecido pelo SCM.
6. A máquina começa a leitura do ficheiro. Podemos confirmar isto com a mensagem ("Generating messages"). Lê as mensagens uma de cada vez e envia-as como *Message Request* para o servidor.
7. O servidor recebe o pedido de mensagem e envia uma resposta a informar do sucesso da operação.
8. A conexão é fechada aquando da finalização da leitura do ficheiro.

#### CASO #2 : Mandar Mensagens Hello Sem Sucesso (SCM desligado) ####

1. Executar o simulador da máquina.
2. O utilizador será notificada que a tentativa de conexão foi recusada devido ao servidor estar desligado.

#### CASO #3 : Mandar Mensagens Hello Sem Sucesso (Máquina não pertence à linha de produção) ####

1. Executar o servidor do Sistema Central de Comunicação com as máquinas.
2. Executar o simulador da máquina (escolher uma máquina que não esteja na linha de produção definida no SCM).
3. O utilizador será notificada que a tentativa de conexão foi recusada devido à máquina que está a ser executada não pertencer à linha de produção definida no SCM.

#### CASO #4 : Mandar Mensagens Hello Sem Sucesso (Máquina não tem certificado válido para estabelecer contacto com o SCM de maneira a enviar mensagens) ####

1. Gerar os certificados através da execução do comando ./make_certs no gitbash.
2. Executar o servidor do Sistema Central de Comunicação com as máquinas.
3. Executar o simulador da máquina (por exemplo, se a linha de produção escolhida for a *LP_002*, executar a máquina 10 cujo certificado seja client50.pem/ client50.key).
4. O utilizador será notificada que a tentativa de conexão foi recusada devido à máquina que está a ser executada não ter um certificado válido.
