# RCOMP 2019-2020 LAPR4 - 1181061 - UC 6002

(Este ficheiro deve ser editado apenas pelo membro 1181061)

## **Visão Geral do Caso de Uso:**

![Main page - Send Restart Request to a Machine](https://bitbucket.org/TiagoMoreiraISEP/lei_isep_2019_20_sem4_2di_1181011_1181012_1181047_1181061_1150/src/master/documentacao/SendRestartRequestToMachine_6002/SendRestartRequestToMachine.md)

## **Explicação Geral**

O caso de uso permite que o sistema de monitorização envie uma mensagem de RESET para uma máquina que está a monitorizar. O SMM apresenta uma lista de máquinas com o IP conhecido pelo SMM, depois, este envia o pacote de dados UDP para a máquina e espera que esta envie uma resposta, podendo esta ser ACK ou NACK, ACK se foi possível estabelecer ligação ao SCM e NACK se o anterior não acontecer.

### Interação com o utilizador

1. Executar o Simulador da Máquina.
2. Executar o Sistema de Comunicação Central.
3. Executar o Sistema de Monitorização com as Máquinas.
4. Enviar um pedido de RESET para a máquina.
5. Esperar por uma resposta por parte da máquina.
