RCOMP 2018-2019 LAPR4 - 1181012 - UC 1013
============================================================
(Este ficheiro deve ser editado apenas pelo membro 1181012)

## **Explicação Geral do Caso de Uso** ##

Neste caso de uso era pretendido proteger todas as comunicações entre o Sistema Central e as máquinas. Desta forma, aplicamos o SSL/TLS com autenticação através de certificados de chave pública. O código desenvolvido no sprint anterior foi reaproveitado, sofrendo algumas alterações. Os dados que eram enviados e recebidos sem segurança, passam a ser enviados e recebidos por métodos da biblioteca SSL/TLS. O servidor tem uma chave privada, enquanto a chave pública irá ser incorporada no certificado SSL e partilhada pelos clientes.

Utilizando o SSL/TLS, podemos garantir uma autenticação sólida entre as duas aplicações de rede. Ambas as aplicações necessitam de ter um certificado de chave pública. Não é relevante se a aplicação é cliente ou servidor, no entanto é necessário ter presente que por omissão o servidor não exige um certificado de chave pública ao cliente, logo para haver autenticação mútua é necessário que a aplicação servidora solicite o certificado do cliente. Geramos os certificados, através do make_certs para que seja feita uma comunicação SSL entre as aplicações. Cada aplicação tem de ter um certificado de chave pública (e a correspondente chave privada). Estes certificados são auto assinados e a informação documental neles constante não é relevante, o importante é a chave pública. 

Para que as aplicações se aceitem mutuamente como autênticas é necessário que:
* No SCM, os certificados de confiança incluam o certificado de chave pública do simulador da máquina.
* No simulador da máquina, os certificados de confiança incluam o certificado de chave pública do SCM.

O certificado de chave pública do simulador da máquina está na key store B.jks, mas pode ser exportado em formato PEM.
O ficheiro .pem tem de ser fornecido ao SCM, sendo assim o único certificado aceite pelo SCM:



### Interação com o utilizador ###

* Gerar os certificados através da execução do comando ./make_certs no gitbash.
* Executar o servidor do Sistema Central de Comunicação com as máquinas.
* Executar o simulador da máquina.
* O simulador da máquina envia um hello request ao servidor.
* O servidor recebe o pedido e retorna um ACK, visto que a máquina pertence à linha de produção identificada no servidor e tem um certificado conhecido pelo SCM.
* A máquina começa a leitura do ficheiro. Podemos confirmar isto com a mensagem ("Generating messages"). Lê as mensagens uma de cada vez e envia-as como Message Request para o servidor.
* O servidor recebe o pedido de mensagem e envia uma resposta a informar do sucesso da operação.
* A conexão é fechada aquando da finalização da leitura do ficheiro.